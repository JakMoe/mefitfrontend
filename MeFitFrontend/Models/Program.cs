﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitFrontend.Models
{
    public class Program
    {
       public int Id { get; set; }
       public string Name { get; set; }
       public  string Category { get; set; }
        public int? ProfileId { get; set; }
       public Profile? Profile { get; set; }

    public ICollection<ProgramWorkout> programWorkouts { get; set; }

    }
}
