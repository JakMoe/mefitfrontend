﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitFrontend.Models
{
    public class MeFitApiDbContext : DbContext
    {

        public MeFitApiDbContext(DbContextOptions<MeFitApiDbContext> options) : base(options) { }

        public DbSet<Profile> Profiles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<Goal> Goals { get; set; }
        public DbSet<Program> Programs { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<GoalWorkout> GoalWorkouts { get; set; }
        public DbSet<ProgramWorkout> ProgramWorkouts { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GoalWorkout>().HasKey(gw => new { gw.GoalId, gw.WorkoutId });
            modelBuilder.Entity<ProgramWorkout>().HasKey(pw => new { pw.ProgramId, pw.WorkoutId });
            modelBuilder.Entity<Profile>().ToTable("Profile");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Goal>().ToTable("Goal");
            modelBuilder.Entity<Address>().ToTable("Address");
            modelBuilder.Entity<Exercise>().ToTable("Exercise");
            modelBuilder.Entity<Set>().ToTable("Set");
            modelBuilder.Entity<MeFitFrontend.Models.Program>().ToTable("Program");
            modelBuilder.Entity<Workout>().ToTable("Workout");


        }
    }
}
