﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitFrontend.Models
{
    public class Workout
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool Complete { get; set; }
        public int ProfileId { get; set; }
        public Profile Profile { get; set; }
        public ICollection<Set> Sets { get; set; }
        public ICollection<ProgramWorkout> programWorkouts { get; set; }
        public ICollection<GoalWorkout> goalworkouts { get; set; }

    }
}
