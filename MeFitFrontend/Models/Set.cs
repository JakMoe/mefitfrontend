﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitFrontend.Models
{
    public class Set
    {
        public int Id { get; set; }
        public int ExerciseReps { get; set; }
        public int ExerciseId { get; set; }
        public Exercise Exercise { get; set; }
        public Profile Profile { get; set; }
    }
}
