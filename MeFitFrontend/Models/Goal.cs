﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitFrontend.Models
{
    public class Goal
    {
        public int Id { get; set; }
        public DateTime Date{get;set;}
        public bool Achieved { get; set; }
        public int ProgramId { get; set; }
        
        public Program Program { get; set; }
        public ICollection<GoalWorkout> goalworkouts { get; set; }


    }
}
