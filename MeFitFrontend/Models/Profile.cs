﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitFrontend.Models
{
    public class Profile
    {
        public int Id { get; set; }
        public int Weight { get; set; }
        public int Height { get; set; }
        public string? MedicalConditions { get; set; }

        public string? Disabilities { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public Address Address { get; set; }
        
        public Program? Program { get; set; }
        public Workout? Workout { get; set; }
    }
}
