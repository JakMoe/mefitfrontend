﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitFrontend.Models
{
    public class ProgramWorkout
    { 
        public int Id { get; set; }
        public int ProgramId { get; set; }
        public Program Program { get; set; }
        public int WorkoutId { get; set; }
        public Workout Workout { get; set; }
    }
}
