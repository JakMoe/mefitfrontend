import React, { Component } from 'react';

export class FetchData extends Component {
  static displayName = FetchData.name;

    constructor(props) {
        super(props);
        this.state = { profiles: [], loading: true, profile:{}
        };
        this.renderProfilesTable = this.renderProfilesTable.bind(this);
  }

  componentDidMount() {
      this.populateProfilesData();

  }

    renderProfilesTable(profiles) {
     console.log(profiles)
    return (
      <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>Name </th>
            <th>Weight </th>
            <th>Height </th>
                    <th>Medical conditions </th>
                    <th>Disabilities </th>
            <th>Actions</th>
          </tr>
        </thead>
            <tbody>
                {profiles.map(profile =>
                    <tr key={profile.id}>
                        <td>{profile.user.firstName}</td>
                        <td>{profile.weight}kg</td>
                        <td>{profile.height}cm</td>
                        <td>{profile.medicalConditions}</td>
                        <td>{profile.disabilities}</td>
                        <td> <button>Edit</button> <button>Delete</button></td>
                    </tr>
                )}
                        
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
        ? <p><em>Loading...</em></p>
        : this.renderProfilesTable(this.state.profiles);

    return (
      <div>
        <h1 id="tabelLabel" >Profiles</h1>
        {contents}
      </div>
    );
  }

  async populateProfilesData() {
    const response = await fetch('api/profiles');
      const data = await response.json();
      this.setState({ profiles: data, loading: false });
  }
}
