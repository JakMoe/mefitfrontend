﻿import React, { Component } from 'react';

export class AddExercise extends Component {
    static displayName = AddExercise.name;

    constructor(props) {
        super(props);
        this.state = {
            name: "", description: "", targetmusclegroup: "", image: "", video: ""
        };
        this.addExercise = this.addExercise.bind(this);
    }
        async addExercise(){
            let exercise = {
                Name: this.state.name,
                Description: this.state.description,
                TargetMuscleGroup: this.state.targetmusclegroup,
                Image: this.state.image,
                VideoLink: this.state.video
            };
            fetch('api/exercises', {
                method: 'POST',
                body: JSON.stringify(exercise),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            }).then(response => response.json()).then(res => {
                this.setState({ exercise: res }); console.log(res);
            })
        }
    

    render() {
    return (
        <div style={{ textAlign: "center" }}>
            <h1>Add Exercise</h1>
            <div style={{ textAlign: "center" }}>
                <form onSubmit={this.addExercise}>
                    <label htmlFor="name">Exercise Name:
                            <input onChange={(event) => this.setState({ name: event.target.value })}
                            type="text"
                            id="name"
                            name="name"
                            required
                        /><br /><br />
                    </label>
                </form>
            </div>
        </div>
    );
      
}
}